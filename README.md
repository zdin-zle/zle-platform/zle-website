# zle-website

The current live version can be found at https://zle.offis.de/. The website describes the idea of the ZLE research and development platform for energy research as introduced by Werth et al. [1]. It will be extended to a platform later on.


## License
Following the ZLE Open Science declaration [2] the website is published under an open license.
All contribution are licensed under the MIT license.


## Literature
[1] O. Werth, S. Ferenz, and A. Nieße, “Requirements for an Open Digital Platform for Interdisciplinary Energy Research and Practice,” Wirtschaftsinformatik 2022 Proceedings, Jan. 2022, [Online]. Available: https://aisel.aisnet.org/wi2022/sustainable_it/sustainable_it/2

[2] S. Ferenz et al., “ZLE Open Science Declaration,” Aug. 2021, doi: 10.5281/zenodo.5221234.
